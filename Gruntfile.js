module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        watch: {
            scripts: {
              files: 'js/*.js',
              tasks: ['eslint'],
              options: {
                interrupt: true,
              },
            },
        },

        eslint: {
            options: {
                configFile: '.eslintrc.js'
            },
            target: ['js/*.js']
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-eslint');
    grunt.loadNpmTasks('grunt-contrib-watch');

};
